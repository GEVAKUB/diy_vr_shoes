# Build your Own! Open-Source VR Shoes for Unity3D

Hand-held controllers enable all kinds of interaction in Virtual Reality (VR), such as object manipulation as well as for locomotion. VR shoes allow to use the hand exclusively for naturally manual tasks, such as object manipulation, while locomotion could be realized through feet input - just like in the physical world. While hand-held VR controllers became standard input devices for consumer VR products, VR shoes are only barely available, and also research on that input modality remains open questions. We contribute here with open-source VR shoes and describe how to build and implement them as Unity3D input device. We hope to support researchers in VR research and practitioners in VR product design to increase usability and natural interaction in VR.

This project shows how to create the simple "walk in place" interface for VR. The physical interface was created with an Arduino UNO. The implementation of the demo application took place in Unity and was tested with Windows 10 and Unity 2018.2.18f1.
As Head Mounted Display (HMD) the Vive headset with OpenVR was used. Steam is required to run OpenVR applications, so the installation of Steam and SteamVR is nessessary in order to run this Unity project. 

## Folder Structure

Project Folder   
	|_ Arduino  
	|_ Fritzing  
	|_ Unity  
	
#### Arduino
* holds the Arduino project for the DIY VR Shoes
* open the script with the Arduino IDE, compile and deploy it to your Arduino board

#### Fritzing
this folder holds the Fritzing project for the electronic circuit and images of the electronic circuit.

#### Unity
This folder holds a simple sample project to show the functionality of the interface. The folder *Scripts* contains the *PlayerMovement.cs* script. This simple script shows how use Ardity (Serial Communication for Arduino + Unity) to connect the Arduino with Unity, process the protocol, and move the Player Object by adding forces to the rigid body of this object.

#### HowTo
* connect the Arduino via USB and your VIVE headset
* mount your soles under your feet
* connect your soles to your Arduino

* open folder Unity in Unity and open Demo VR Scene (*Assets -> Scenes -> Demo VR Scene.scene*)
* select GameObject Player in hierarchy tree
* in inspector set:
	* Move Speed (speed of movement for one step)
	* Eye Height (height of the HMD, measured from ground, in m)
	* Arduino Port (the COM-Port of your Arduino)
	* Arduino Baud Rate (baud rate of your Arduino, if you don’t change the Arduino script, 9600 baud is perfect)
* press play and walk in place

## Electronic Components

![alt text](Fritzing/prototype.png "Electronic circuit")

To recreate the soles the following electronic components are needed:

* 2* Interlink Electronics FSR 402 (A)
* 2* jack plug and  (B)
* 1* Arduino UNO (C)
* 1* USB cable (D)
* 2* Resistor 10kOhm (E)
* wires and connectors to Arduino
* 1* housing
* 2* soles and straps for mounting

Connect the electronic components to the Arduino as indicated in the wiring diagram.

![alt text](Fritzing/Shoe_Sketch_Steckplatine2.png "Electronic circuit")

## Third Party content
### Code
This project uses Ardity to communicate with the Arduino.
* Ardity (https://github.com/dwilches/Ardity, License: ([Attribution 2.0 Generic (CC BY 2.0)](https://creativecommons.org/licenses/by/2.0/)))

#### Sounds
The sounds used in the Unity project are taken from http://soundbible.com and https://freesound.org.

* Wind-Mark_DiAngelo-1940285615.wav (http://soundbible.com/1810-Wind.html, License: [Attribution 3.0 Unported (CC BY 3.0)](https://creativecommons.org/licenses/by/3.0/), Recorded by Mark DiAngelo)
* Snow steps (https://freesound.org/people/mltrig/sounds/220785/, License: [CC0 1.0 Universal (CC0 1.0), Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/), Recorded by mltrig)

#### Textures
The textures for the terrain are taken from https://opengameart.org. The blizzard textures where made by our own.

* water(1).png (https://opengameart.org/node/8081, License: [Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)](https://creativecommons.org/licenses/by-sa/3.0/))
* 461223156.jpg (https://opengameart.org/node/7866, License: [CC0 1.0 Universal (CC0 1.0), Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/))
* snow1.jpg (https://opengameart.org/node/7408, , License: [CC0 1.0 Universal (CC0 1.0), Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/))
## Acknowledgments
The GEVAKUB project underlying this work was funded by the German Federal Ministry of Education and Research, funding code 01JKD1701B.





	




