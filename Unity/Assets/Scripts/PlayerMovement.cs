﻿using UnityEngine;
using System.IO.Ports;
using UnityEngine.XR;
using System.Collections;
using System;

/// <summary>
/// Simple script that connects to the Arduino on the given port and controls a 
/// CharacterController by the input from Arduino
/// </summary>
public class PlayerMovement : MonoBehaviour {

    // enable or disable positional tracking of HMD (only on start)
    public bool disablePositionalTracking = true;

    // factor for moving for one step
    public float moveSpeedMultiplicator = 3.0f;

    // because no positional tracking, set height of players eyes
    public float eyeHeight = 1.7f;

    // Array to hold different sounds for steps
    public AudioClip[] footstepSounds;
    // Step AudioSource
    public AudioSource stepSound;

    // SerialController to handle communication with Arduino
    public SerialController serialController;

    // Use this for initialization
    void Start()
    {
        serialController = this.GetComponent<SerialController>();
        InputTracking.disablePositionalTracking = disablePositionalTracking;
        Camera.main.transform.localPosition = new Vector3(0, eyeHeight, 0);
    }

    /// <summary>
    /// Update is called once per frame and reads serial messages from SerialController.
    /// 
    /// </summary>
    void Update() {
 
        string message = serialController.ReadSerialMessage();

        if (message == null)
            return;

        // Check if the message is plain data or a connect/disconnect event.
        if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            Debug.Log("Connection established");
        else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
            Debug.Log("Connection attempt failed or disconnection detected");
        else
        {
            //Debug.Log("Message arrived: " + message);
            int msg;
            if (int.TryParse(message, out msg))
                ArduinoMovement(msg);


        } 
    }


        /// <summary>
        /// Method implements movement for arduino soles
        /// </summary>
        /// <param name="pressureStatus"></param>
    void ArduinoMovement(int pressureStatus)
    {
        Vector3 moveDirection = Camera.main.transform.forward;
        Vector3 vel = new Vector3(moveDirection.x, 0, moveDirection.z);
        vel.Normalize();

        // walking recognized
        if (pressureStatus == 1 || pressureStatus == 2)
        {
            GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * moveSpeedMultiplicator, ForceMode.VelocityChange);
            PlayFootStepAudio();            
        }
    }

    /// <summary>
    /// play randomly some step sounds
    /// </summary>
    void PlayFootStepAudio()
    {

        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = UnityEngine.Random.Range(1, footstepSounds.Length);
        stepSound.clip = footstepSounds[n];
        stepSound.PlayOneShot(stepSound.clip);

        // move picked sound to index 0 so it's not picked next time

        footstepSounds[n] = footstepSounds[0];
        footstepSounds[0] = stepSound.clip;
    }
}
