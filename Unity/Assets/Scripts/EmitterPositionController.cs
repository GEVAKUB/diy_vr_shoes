﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


     /// <summary>
     /// Script moves emitter together with the player or camera.
     /// assign the player or camera object to the player property. 
    /// 
    /// </summary>
public class EmitterPositionController : MonoBehaviour
{


    public Transform player;

    // Use this for initialization
    void Start()
    {
        if (player == null)
            Debug.Log("Please attach player object or camera parent object to this script.");

    }

    // Update is called once per frame
    void Update()
    {
        if (player != null)
            transform.position = player.position;

    }
}