/*  
 *  GEVAKUB: 
 *  Shoe with force sensitive resistor (FSR) on each heel
 * 
 */

// analog pins for FSR
int PinLeft = 0;
int PinRight = 5;

// the analog reading from the FSR resistor divider
int fsrReadingLeft;
int fsrReadingRight;

// presure needed for activation
int pressureLeft = 50;
int pressureRight = 50;

//Bool to detect if both feet were on the ground
bool ground = false;

//toggle debug mode
boolean debugmode = false;

//last output
int lastOutput = -1;

// send debugging information via the Serial Monitor
void setup(void) 
{
  Serial.begin(9600);
}

// send information about pressure to Unity
void loop(void) 
{
   int newOutput = 0;
   
   fsrReadingLeft = analogRead(PinLeft);
   fsrReadingRight = analogRead(PinRight);

   if (fsrReadingLeft > pressureLeft)
   {
     if(fsrReadingRight > pressureRight)
     {
        //both feet touch the ground
        newOutput = 3;
        ground = true;
     }
   }
   if (ground && fsrReadingLeft > pressureLeft)
      {
      if (fsrReadingRight < pressureRight)
          {
            //left Foot touches the ground
            newOutput = 1;
          }
      }
    else if (ground && fsrReadingLeft < pressureLeft)
            {
              if (fsrReadingRight > pressureRight)
                  {
                    //right foot touches the ground
                    newOutput = 2;
                  }
              else
                  {
                    //no foot touches the ground
                    newOutput = 0;
                    ground = false;
                  }
            }
            

      
   
   if(newOutput != lastOutput)
   {
      Serial.flush(); 
      Serial.println(newOutput);
      lastOutput = newOutput;
   }

   if(debugmode)
   {
     Serial.print("\n");
     Serial.print("#######################################");
     Serial.print("\n");
     Serial.print("Pressure on right foot(A) : \t");
     Serial.print(fsrReadingRight);
     Serial.print("\n");
     Serial.print("Pressure on left foot(B) : \t");
     Serial.print(fsrReadingLeft);
     Serial.print("\n");
     Serial.print("New Output : \t");
     Serial.print(newOutput);
     Serial.print("\n");
     Serial.print("Last Output : \t");
     Serial.print(lastOutput);
     Serial.print("\n");
     Serial.print("#######################################");
     Serial.print("\n");
     delay(1000);
   }
  delay(50);
}
